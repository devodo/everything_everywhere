import argparse
import requests
from filecache import filecache
from bs4 import BeautifulSoup
# from multiprocessing import Process
import sys
sys.setrecursionlimit(10000)  # Hacky

SCRAPE_RANK_URL = 'http://www.socialsecurity.gov/cgi-bin/babyname.cgi'
SCRAPE_DATA_URL = 'http://www.socialsecurity.gov/cgi-bin/popularnames.cgi'


def parse_args():
    """
    Start by registering and accepting some cmd line args
    """

    parser = argparse.ArgumentParser(description='Everything Everywhere CLI tool\
        ')
    parser.add_argument('name', help='Specify a name')
    parser.add_argument(
        'year_from',
        type=int,
        choices=range(1880, 2014),
        help='Sort by year to',
    )
    parser.add_argument(
        'year_to',
        type=int,
        choices=range(1880, 2014),
        help='Sort by year from',
    )
    return parser.parse_args()


@filecache(86400 * 7)
def fetch_year_rank(year, rank):
    """
    Returns the amount of births at the given rank
    for the given year.
    """

    year_rank_response = requests.post(SCRAPE_DATA_URL, data={
        'top': '1000',
        'number': 'n',
        'year': year
    })

    soup = BeautifulSoup(year_rank_response.text, "html.parser")

    return int(
        float(
            soup.findAll('table')[1]
            .findAll('table')[0]
            .findAll('tr')[int(rank)]
            .findAll('td')[2]
            .contents[0]
            .replace(',', '')
        )
    )


@filecache(86400 * 7)
def fetch_init(options):
    """
    Returns the given rank for each year
    in the range specified
    """

    # Fetch the babynames url
    return requests.post(SCRAPE_RANK_URL, data={
        'name': options.name,
        'year': options.year_to,
        'start': options.year_from,
        'number': 'n',
        'sex': 'M'
    })


def fetch_results(options):
    """
    Fetch the babynames scrape URL given the options passed

    # We know a basic element looks like this
    # <table width=96% class=layout>
    #     <tr valign="bottom">
    # 		<td class="nobarborder" width="8%" align="center">1998</td>
    # 		<td class="nobarborder" width="8%" align="center">332</td>
    # 		<td class=bluebar width=56.196%>&nbsp;</td>
    # 		<td width=27.804%></td>
    #     </tr>
    # </table>

    """

    print "Performing lookup. Total births for %s between %s and %s" % (
        options.name,
        str(options.year_from),
        str(options.year_to)
    )

    response = fetch_init(options)

    # Total number of births counter
    total_number_of_births = 0

    # Soupify the request data
    soup = BeautifulSoup(response.text, "html.parser")

    # Reverse out the years so it reads from/to
    for row in reversed(soup.findAll('table', {"class": "layout"})):
        if not row.find('td'):
            continue
        table_cells = row.findAll('td')
        year = table_cells[0].contents[0]
        rank = table_cells[1].contents[0]

        # If unable to select either rank or year move on
        if not year and not rank:
            continue

        # If the given year is outside of the range, break
        if int(year) > int(options.year_to):
            break

        # Fetch the number of births
        births_this_year = fetch_year_rank(year, rank)
        total_number_of_births += births_this_year

        print "Total births for %s in %s: %s" % (
            options.name,
            year,
            str("{:,}".format(births_this_year))
        )

    print "The total number of births for %s between the years\
 %s and %s is %s" % (
        options.name,
        options.year_from,
        options.year_to,
        str("{:,}".format(total_number_of_births))
    )

if __name__ == '__main__':
    fetch_results(parse_args())
