# Getting started

### Mac OS X instructions

sudo easy_install pip
sudo pip install virtualenv virtualenvwrapper

#### Environment settings
```
    # set where virutal environments will live
    export WORKON_HOME=$HOME/.virtualenvs
    # ensure all new environments are isolated from the site-packages directory
    export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
    # use the same directory for virtualenvs as virtualenvwrapper
    export PIP_VIRTUALENV_BASE=$WORKON_HOME
    # makes pip detect an active virtualenv and install to it
    export PIP_RESPECT_VIRTUALENV=true
    if [[ -r /usr/local/bin/virtualenvwrapper.sh ]]; then
        source /usr/local/bin/virtualenvwrapper.sh
    else
        echo "WARNING: Can't find virtualenvwrapper.sh"
    fi
```

#### virtualenv
```workon```
```mkvirtualenv ee```

#### Install modules
pip install -r requirements.txt

#### Running the command
```python ee.py [Name] [Year_from] [Year_to]```


# Improvements
Use multithreaded approach to url processing.
Haven't quite got around to doing this just yet.
I've not had as much time as I would like to work on this test as it is.
In the essence of time, we can discuss it rather perhaps when talking it over.
